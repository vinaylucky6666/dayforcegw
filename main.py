from flask import Flask,request,jsonify
from concurrent.futures import ThreadPoolExecutor
from get_employee_details import get_employee_details
from custom_exception import CustomException
from dayforce_connector import *
from datetime import datetime
from config import *
import re
import arrow
from logger import log


app = Flask(__name__)


@app.route('/v1/dayforceGW/employeeDetails/', methods=['GET'])
def client_request():
    try:
        log.info('Request_headers : %s', request.environ)
        start_time = datetime.now()
        from_date_param = request.args.get('fromDate')
        to_date_param = request.args.get('toDate')
        if not from_date_param or not to_date_param:
            error = {"Results": {"Error": "Parameter should contain 'fromDate' and 'toDate' ", "code": 400,
                                 "context": "BadRequest"}}
            log.error("Parameter should contain 'fromDate' and 'toDate': %s", error)  # logger.error() call
            return jsonify(error), 400
        emp_codes_details = {}
        try:
            contextdateObject = dayforce_connector()  # created context object to call context method
            context_response = contextdateObject.get_contextdate_details(
                datetime.now().strftime('%Y-%m-%d %H:%M:%S'))  # context response as list of XRefCodes
            total_emp_codes = []
            for k in range(0, len(context_response['Data'])):
                if re.search("INVALID", context_response["Data"][k]["XRefCode"]) is None:
                    total_emp_codes.append(context_response["Data"][k]["XRefCode"])
            emp_codes_details['Result'] = total_emp_codes
        except Exception as e:
            logger.log.exception('Exception in context response', exc_info=True)

        total_emp_codes = emp_codes_details['Result']
        employee_details = []

        while len(total_emp_codes) != 0:
            try:
                log.info("Total employees being processed:%s",len(total_emp_codes))
                result_list = []
                i = 0
                while i < len(total_emp_codes):  # len(context_response['Data'])
                    with ThreadPoolExecutor(max_workers=NUMBER_OF_THREADS) as executor:  # will execute no.of threads at a time
                        for j in range(0, NUMBER_OF_THREADS):  # run no of thread
                            if i + j < len(total_emp_codes):  # len(context_response['Data'])
                                result_list.append(executor.submit(get_employee_details, total_emp_codes[i + j],i + j, from_date_param, to_date_param))  # limited_xref[i+j][0]
                        executor.shutdown(wait=True)
                    i = i + NUMBER_OF_THREADS
                total_emp_codes = []
                for x in result_list:
                    if x.result() is not None:
                        if type(x.result()) is dict:
                            employee_details += [x.result()]
                        else:
                            total_emp_codes.append(x.result())
            except Exception as e:
                logger.log.exception('Exception in thread executor', exc_info=True)
        log.info('Total no.of employees processed:%s',len(employee_details))
        final_emp_details = {'Results': employee_details}
        end_time = datetime.now()
        log.info('Processing started at: %s', start_time.strftime('%Y-%m-%d %H:%M:%S'))
        log.info('Processing end time: %s', end_time.strftime('%Y-%m-%d %H:%M:%S'))
        process_time = (arrow.get(str(end_time)) - arrow.get(str(start_time)))
        log.info('Total processing time: %s', process_time.seconds)
        return jsonify(final_emp_details)
    except Exception as e:
        log.exception('Exception by occurs',exc_info=True)
        raise Exception(e.args)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
