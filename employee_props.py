import logger
from config import country as country_map

def employee_prop(employee_res_details):
    activity_type = cost_center = company_code = country = Is_manager = ''
    try:
        if len(employee_res_details['Data']['EmployeeProperties']['Items']) == 0:
            return Is_manager, cost_center, company_code, country, activity_type
        for j in range(0, len(employee_res_details['Data']['EmployeeProperties']['Items'])):
            employee_props = employee_res_details['Data']['EmployeeProperties']['Items'][j]['EmployeeProperty'][
                'XRefCode']
            if employee_props == 'EmployeePropertyXrefCode52':
                if 'StringValue' in employee_res_details['Data']['EmployeeProperties']['Items'][j]:
                    activity_type = employee_res_details['Data']['EmployeeProperties']['Items'][j]['StringValue']
            if employee_props == 'EmployeePropertyXrefCode1':
                if 'StringValue' in employee_res_details['Data']['EmployeeProperties']['Items'][j]:
                    cost_center = employee_res_details['Data']['EmployeeProperties']['Items'][j]['StringValue']
            if employee_props == 'EmployeePropertyXrefCode58':
                if 'OptionValue' in employee_res_details['Data']['EmployeeProperties']['Items'][j] \
                        and 'XRefCode' in employee_res_details['Data']['EmployeeProperties']['Items'][j]['OptionValue']:
                    company_code = employee_res_details['Data']['EmployeeProperties']['Items'][j]['OptionValue'][
                    'XRefCode']
                    country = country_map[company_code]
            if employee_props == 'EmployeePropertyXrefCode27':
                if 'OptionValue' in employee_res_details['Data']['EmployeeProperties']['Items'][j] \
                        and 'XRefCode' in employee_res_details['Data']['EmployeeProperties']['Items'][j]['OptionValue']:
                    Is_manager = employee_res_details['Data']['EmployeeProperties']['Items'][j]['OptionValue'][
                    'XRefCode']
        return Is_manager, cost_center, company_code, country, activity_type
    except Exception as e:
        logger.log.exception('Exception in mapping company_code,country,cost_center,Is_manager',exc_info=True)
        return







