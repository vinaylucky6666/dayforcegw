from config import *
import requests,logger
from requests.auth import HTTPBasicAuth
from google.cloud import datastore


class dayforce_connector:
    credentials = {} 
    def __init__(self):
        if self.credentials == {}:
            query = datastore.Client().query(kind=KIND)
            for key,value in list(query.fetch())[0].items():
                self.credentials[key] = value

    def get_employee_details(self, xRefCode):
        try:
            # Connect to the DayForce API to get employee response for employee details
            employee_resp = requests.get(url=EMPLOYEE_URL.format(xRefCode), auth=self.get_authorization())
            logger.log.info('Employee response status_code is %s', employee_resp.status_code)
            employee_details_json=employee_resp.json()

            if employee_resp.status_code == 200:
                return employee_details_json  # returns employee response
            elif employee_resp.status_code == 500:
                logger.log.warning('%s-->employee in the list as duplicate XRefcode with status_code:500', xRefCode)
                return "500"
            elif employee_resp.status_code == 429:
                logger.log.warning('%s--> as too many requests error from sever for XRefcode with status_code:429', xRefCode)
                return "429"
            else:
                return {}  # returns empty dictonary
        except Exception as e:
            logger.log.exception('Employee details response exception', exc_info=True)
            logger.log.info('This employee: %s will pass with no data', xRefCode)
            return

    def get_contextdate_details(self,as_of_date):
        try:
            # Connect to the DayForce API to get contextDate response for employee XRefCode list
            contextdate_resp = requests.get(url=CONTEXTDATE_URL.format(as_of_date), auth=self.get_authorization())
            contextdate_details_json = contextdate_resp.json()
            return contextdate_details_json  # return contextdate response
        except Exception as e:
            logger.log.exception('Employees list ContextDate response exception', exc_info=True)
            return


    def get_authorization(self):
        return HTTPBasicAuth(self.credentials['DAYFORCE_USERNAME'], self.credentials['DAYFORCE_PASSWORD'])  # return HTTPBasicAuth details

    def get_manager_details(self, manager_xrefcode):
        try:
            resp_details = {}
            if manager_xrefcode != None:
                # Connect to the DayForce API to get manager response for manager details
                manager_resp = requests.get(url = MANAGER_URL.format(manager_xrefcode), auth = self.get_authorization())
                logger.log.info('Manager response status_code is %s', manager_resp.status_code)
                resp_details = manager_resp.json()
                if manager_resp.status_code == 200:
                    return resp_details  # return manager response details
                elif manager_resp.status_code == 500:
                    return "500"
                elif manager_resp.status_code == 429:
                    return "429"
                else:
                    return {}
            return resp_details  # return empty string
        except Exception as e:
            logger.log.exception('Manager details response exception', exc_info=True)
            logger.log.info('This manager: %s will pass with no data', manager_xrefcode)
            return
