import logger
def employee_contact(employee_res_details):
    Employee_email = ''
    try:
        if len(employee_res_details['Data']['Contacts']['Items']) == 0:
            return Employee_email
        for h in range(0, len(employee_res_details['Data']['Contacts']['Items'])):
            if 'ContactInformationType' in employee_res_details['Data']['Contacts']['Items'][h] \
                    and 'ContactInformationTypeGroup' in employee_res_details['Data']['Contacts']['Items'][h]['ContactInformationType'] \
                    and 'XRefCode' in employee_res_details['Data']['Contacts']['Items'][h]['ContactInformationType'][
                'ContactInformationTypeGroup']:
                contact_email= employee_res_details['Data']['Contacts']['Items'][h]['ContactInformationType'][
                'ContactInformationTypeGroup']['XRefCode']  # mapping employee email
                if contact_email == 'ElectronicAddress':
                    if 'ElectronicAddress' in employee_res_details['Data']['Contacts']['Items'][h]:
                        Employee_email = employee_res_details['Data']['Contacts']['Items'][h]['ElectronicAddress']
        return Employee_email  # return employee email
    except Exception as e:
        logger.log.exception('Exception in mapping employee contact_email', exc_info=True)
        return
