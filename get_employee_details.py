from concurrent.futures import ThreadPoolExecutor
from employee_props import employee_prop
from employee_stats import employee_stat
from employee_contc import employee_contact
from custom_exception import CustomException
from dayforce_connector import *
from employee_details import *
import logger


def get_employee_details(xRefCode, i, from_date, to_date):
    # process check (make sure invalid return process done----------)
    try:
        try:
            employee_object = dayforce_connector()  # created object to call employee_response method
            employee_res_details = employee_object.get_employee_details(xRefCode=xRefCode)  # we get the employee response
        except Exception as e:
            logger.log.exception('Exception in DayForceAPI conne', exc_info=True)
            return
        if employee_res_details == "429":  # {} when employee response status code other than 200
            return xRefCode # ------------------------------------->skip>--when duplicate xrefcode occurs
        if employee_res_details == "500":
            return
        if employee_res_details == {}:
            logger.log.warning('%s-->employee in the list as duplicate XRefcode with status_code:UNKNOWN', xRefCode)
            return
        if "EffectiveStart" in employee_res_details['Data']['EmploymentStatuses']['Items'][0]:
            start_date = employee_res_details['Data']['EmploymentStatuses']['Items'][0]['EffectiveStart']
        else:
            return
        if start_date < from_date or start_date > to_date:
            return
        employee_group, employee_status, action_type = employee_stat(employee_res_details)  # call employee_stat method
        if employee_group == '' or employee_status == '':
            logger.log.warning('%s employee as not mapped employee group to 1 or D', xRefCode)
            return  # -------------------------------->skip if employee group or status is null
        # thread function calls
        executors_list = []  # executor details will append to the list
        try:
            with ThreadPoolExecutor(max_workers=5) as executor:
                executors_list.append(executor.submit(employee_prop, employee_res_details))
                executors_list.append(executor.submit(employee_contact, employee_res_details))
            employee_thr_details = []
            for x in executors_list:
                employee_thr_details += [x.result()]  # final thread result will append to the list.
        except Exception as e:
            logger.log.exception('Exception in thread executors', exc_info=True)
            return

        try:
            if len(employee_res_details['Data']['EmployeeManagers']['Items']) != 0:
                manager_xrefcode = employee_res_details['Data']['EmployeeManagers']['Items'][0]['ManagerXRefCode']
                manager_details = employee_object.get_manager_details(manager_xrefcode=manager_xrefcode)  # will get response
                if manager_details == "429":  # {} when employee response status code other than 200
                    logger.log.warning('%s-->manager employee in the list XRefcode with status_code:429', xRefCode)
                    return xRefCode  # ------------------------------------->skip>--when duplicate xrefcode occurs
                if manager_details == "500":
                    logger.log.warning('%s-->manager employee in the list as duplicate XRefcode with status_code:500', xRefCode)
                    return
                if manager_details == {}:
                    logger.log.warning('%s--> manager employee in the list as duplicate XRefcode with status_code:UNKNOWN', xRefCode)
                    return
            else:
                manager_details = employee_object.get_manager_details(manager_xrefcode=None)  # will get ''
        except Exception as e:
            logger.log.exception('Exception in manager response', exc_info=True)
            return

        details_object =employees_basic_details()
        employee_details = details_object.employee_details(employee_res_details,employee_thr_details,employee_group,manager_details)
        # employee payload
        employee_payload = {"employee_number": employee_details[2],
                               "first_name": employee_details[3],
                               "last_name": employee_details[4],
                               "preferred_name": employee_details[5],
                               "work_location": employee_details[6],
                               'department': employee_details[7],
                               "communication_id": employee_thr_details[1],
                               "country": employee_thr_details[0][3],
                               "company_code": employee_thr_details[0][2],
                               "cost_center": employee_details[0],
                               "employee_status": employee_status,
                               "Action_Type": action_type,
                                "Activity_Type":employee_thr_details[0][4],
                               "Is_manager": employee_thr_details[0][0],
                               'employee_group': employee_group,
                               'nationality': '',
                               'date_of_birth': "01-01-2000",
                               'End_Date': "31-12-9999",
                               'Gender': 'M',
                               "Start_Date": employee_details[8],
                                "State":employee_details[1],
                               'User_Id': employee_details[2],
                               'manager_name': employee_details[9],
                               'manager_employee_number': employee_details[10],
                               'manager_email': employee_details[11],
                               }
        return employee_payload
    except Exception as e:
        logger.log.exception('Exception in DayForceAPI connection', exc_info=True)
        logger.log.info('Exception details: %s', e.args)
        return
