EMPLOYEE_URL = "https://globaleurr57.dayforcehcm.com/api/MediaKind/V1/Employees/{}?expand=EmployeeManagers," \
               "EmploymentStatuses,WorkAssignments,EmployeeProperties,Contacts"
MANAGER_URL = "https://globaleurr57.dayforcehcm.com/api/MediaKind/V1/Employees/{}?expand=EmployeeManagers," \
              "EmployeeProperties"
KIND = 'credentials'
NUMBER_OF_THREADS = 15
CONTEXTDATE_URL="https://globaleurr57.dayforcehcm.com/Api/mediakind/V1/Employees?contextDate={}"

country = {"3310": "ARE", "3010": "AUS", "1410": "BRA", "2910": "CAN", "2210": "CHE", "2010": "CHL", "2110": "COL",
            "2410": "ESP", "1220": "FRA", "1120": "UK", "6210": "HKG", "3110": "ISR", "1510": "JPN", "5510": "MEX",
            "3710": "NLD", "3210": "PRT", "3410": "RUS", "6110": "SNG", "6310": "TWN", "1310": "CHN", "1810": "IND",
            "4310": "PRK", "1710": "USA"}



state_code ={"USA-AZ Phoeni": "AZ","USA-CA El Seg": "CA","USA-CA Sacram": "CA",
        "USA-CA Sacrem":"CA","USA-CA Santa": "CA","USA-Californi":"CA",
        "z.USA-CA El S": "CA","z.USA-CA Sacr": "CA","z.USA-CA Sant": "CA",
        "z.USA-Califor": "CA","USA-CO Aspen": "CO","USA-CO Highla": "CO",
        "USA-CO-Denver": "CO","USA-CO-Remote": "CO","z.USA-CO Aspe": "CO",
        "USA-CT Hartfo": "CT","USA-FL Tallah": "FL","z.USA-FL Tall": "FL",
        "USA-GA Atlant": "GA","USA-GA Duluth": "GA","z.USA-GA Atla": "GA",
        "z.USA-GA Dulu": "GA","USA-MA Acton": "MA","USA-MA-Acton": "MA",
        "z.USA-MA Acto": "MA","z.USA-MA-Acto": "MA","USA-NV-Carson": "NV",
        "USA-NH Remote": "NH","USA-NJ Trento": "NJ","z.USA-NJ Tren":"NJ",
        "USA-NY-Remote": "NY","USA-NC-Ashvil": "NC","CAN-ON Missis": "ON",
        "CAN-ON-Ottawa": "ON","CAN-ON-Toront": "ON","CAN-QC Montre": "QC","CAN-QC-Saint":"QC",
        "z.CAN-QC Mont": "QC","z.CAN-QC-Sain": "QC","USA-SC-Myrtle": "SC",
        "USA-Tennesse-": "TN","USA-Tennessee": "TN","USA_TX-Irving": "TX","USA-TX Austin": "TX",
        "USA-TX Frisco": "TX","USA-TX Plano": "TX","USA-TX-Irving": "TX","z.USA-TX Aust": "TX",
        "z.USA-TX Fris": "TX","z.USA-TX-Irvi": "TX","USA-VA-Remote": "VA","USA-WA-Olympi": "WA",
        "z.USA-WA-Olym": "WA","USA-Wisconsin": "WI"}