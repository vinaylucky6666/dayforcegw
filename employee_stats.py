import logger
def employee_stat(employee_res_details):
    global action_type, employee_group, employee_status
    try:
        employee_group = employee_status = action_type = ''

        for l in range(0, len(employee_res_details['Data']['EmploymentStatuses']['Items'])):
            if 'EmployeeGroup' in employee_res_details['Data']['EmploymentStatuses']['Items'][l]\
                    and 'ShortName' in employee_res_details['Data']['EmploymentStatuses']['Items'][l]['EmployeeGroup']:
                employee_grup = employee_res_details['Data']['EmploymentStatuses']['Items'][l]['EmployeeGroup']['ShortName']
                #   employee_group
                if employee_grup == 'Employee' or employee_grup == 'Intern' or employee_grup == 'TSA':
                     employee_group = 1
                elif employee_grup == 'Consultant/Contractor':
                     employee_group = 'D'

            #  employee status
            if 'EmploymentStatus' in employee_res_details['Data']['EmploymentStatuses']['Items'][l]\
                    and 'LongName' in employee_res_details['Data']['EmploymentStatuses']['Items'][l]['EmploymentStatus']:
                employee_stat = employee_res_details['Data']['EmploymentStatuses']['Items'][l]['EmploymentStatus']['LongName']
                if employee_stat == 'Active' or employee_stat == 'Inactive':
                    employee_status = 'Active'
                    action_type = 'Add'
                elif employee_stat == 'Terminated':
                    employee_status = 'Inactive'
                    action_type = 'Remove'
        return employee_group, employee_status, action_type
    except Exception as e:
        logger.log.exception('Exception in mapping employee group,status, action_type', exc_info=True)
        return
