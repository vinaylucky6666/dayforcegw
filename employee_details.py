import logger
from config import state_code as state_code

class employees_basic_details:
    def employee_details(self,employee_res_details,employee_thr_details,employee_group,manager_details):
        employee_number = employee_first_name = employee_last_name = employee_name = manager_name = manager_number = manager_email = work_location = department = start_date = cost_center = state  = ''
        cost_center = employee_thr_details[0][1]
        work_details = employee_res_details['Data']['HomeOrganization']['ShortName']
        if work_details[:13] in state_code:
            state = state_code[work_details[:13]]
        if employee_group == 'D':
            cost_center = ''
        if "EmployeeNumber" in employee_res_details['Data']:
            employee_number = employee_res_details['Data']['EmployeeNumber']
        if "FirstName" in employee_res_details['Data']:
            employee_first_name = employee_res_details['Data']['FirstName']
        if "LastName" in employee_res_details['Data']:
            employee_last_name = employee_res_details['Data']['LastName']
        if "DisplayName" in employee_res_details['Data']:
            employee_name = employee_res_details['Data']['DisplayName']
        if "HomeOrganization" in employee_res_details['Data'] and "ShortName" in employee_res_details['Data'][
            'HomeOrganization']:
            work_location = employee_res_details['Data']['HomeOrganization']['ShortName']
        if "Position" in employee_res_details['Data']['WorkAssignments']['Items'][0] and "Department" in \
                employee_res_details['Data']['WorkAssignments']['Items'][0]['Position'] and 'LongName' in \
                employee_res_details['Data']['WorkAssignments']['Items'][0]['Position']['Department']:
            department = employee_res_details['Data']['WorkAssignments']['Items'][0]['Position']['Department'][
                'LongName']
        if "EffectiveStart" in employee_res_details['Data']['EmploymentStatuses']['Items'][0]:
            start_date = employee_res_details['Data']['EmploymentStatuses']['Items'][0]['EffectiveStart']
        if manager_details != {}:
            logger.log.info('Manger--- details exist ')
            if "DisplayName" in manager_details['Data']:
                manager_name = manager_details['Data']['DisplayName']
            if "EmployeeNumber" in manager_details['Data']:
                manager_number = manager_details['Data']['EmployeeNumber']
            if "FederatedId" in manager_details['Data']:
                manager_email = manager_details['Data']['FederatedId']
        return cost_center, state,employee_number, employee_first_name, employee_last_name, employee_name, work_location, department, start_date, manager_name, manager_number, manager_email